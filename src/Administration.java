import java.time.LocalDate;

public class Administration
{
   Patient patient;  // The currently selected patient
   boolean zv;       // true when 'zorgverlener'; false otherwise

   // Constructor
   Administration( int userID )
   {
      // Initialise (list of) patient(s).
      System.out.format( "Hard-coded profiles!\n" );
      patient = new Patient( 1, "Van Puffelen", "Adriaan", LocalDate.of( 2000, 02, 29 ) );

      zv = (userID == 0);
   }

   // Main menu
   void menu()
   {
      final int STOP  = 0;
      final int PRINT = 2;
      final int EDIT  = 4;

      var scanner = new BScanner();

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 80 ) );
         System.out.format( "Current patient: " );
         patient.writeOneliner();

         ////////////////////////
         // Print menu on screen
         ////////////////////////
         System.out.format( "%d:  STOP\n", STOP );
         System.out.format( "%d:  Print patient data\n", PRINT );
         System.out.format( "%d:  Edit  patient data\n", EDIT );
         ////////////////////////

         System.out.println( "enter digit:" );
         int choice = scanner.scanInt();
         switch (choice)
         {
            case STOP: // interrupt the loop
               nextCycle = false;
               break;

            case PRINT:
               patient.write();
               break;

            case EDIT:
               patient.editMenu( zv ); // Could/Should change when multiple types of zv.
               break;

            default:
               System.out.println( "Please enter a *valid* digit" );
               break;
         }
      }
   }
}
