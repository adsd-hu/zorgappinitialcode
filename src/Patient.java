import java.time.LocalDate;
import java.time.Period;

public class Patient
{
   private final int RETURN      = 0;
   private final int SURNAME     = 1;
   private final int FIRSTNAME   = 2;
   private final int NICKNAME    = 3;
   private final int DATEOFBIRTH = 4;
   private final int LENGTH      = 5;
   private final int WEIGHT      = 6;

   private final String surName;
   private final String firstName;
   private final String nickName;

   private final LocalDate dateOfBirth;

   private double weight = 0.0;
   private double length = -1.0;
   private int    id     = -1;

   // Constructor
   Patient( int id, String surName, String firstName, LocalDate dateOfBirth )
   {
      this.id          = id;
      this.surName     = surName;
      this.firstName   = firstName;
      this.nickName    = firstName; // set via separate method if needed.
      this.dateOfBirth = dateOfBirth;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   Patient( int id, String surName, String firstName, LocalDate dateOfBirth, double length, double weight )
   {
      this( id, surName, firstName, dateOfBirth );
      this.length = length;
      this.weight = weight;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public double getLength()
   {
      return length;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void setLength( double length )
   {
      this.length = length;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public double getWeight()
   {
      return weight;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void setWeight( double w )
   {
      weight = w;
   }

   // Access surName
   public String getSurName()
   {
      return surName;
   }

   // Access surName
   public String getFirstName()
   {
      return firstName;
   }

   // Access callName
   public String getNickName()
   {
      return nickName;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public double calcBMI()
   {
      // YOUR CODE HERE
      return 0.0;
   }

   // Handle editing of patient data
   void editMenu( boolean zv )
   {
      var scanner1 = new BScanner(); // use for numbers.
      var scanner2 = new BScanner(); // use for strings

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.println( "Patient data edit menu:" );
         printEditMenuOptions();
         System.out.println( "Enter digit (0=return)" );

         int choice = scanner1.scanInt();
         switch (choice)
         {
            case RETURN:
               nextCycle = false;
               break;

            case SURNAME:
               System.out.format( "Enter new surname: (was: %s)\n", surName );
               // YOUR CODE  HERE
               break;

            case FIRSTNAME:
               System.out.format( "Enter new first name: (was: %s)\n", firstName );
               // YOUR CODE  HERE
               break;

            case NICKNAME:
               System.out.format( "Enter new nickname: (was: %s)\n", nickName );
               // YOUR CODE  HERE
               break;

            case DATEOFBIRTH:
               System.out.format( "Enter new date of birth (yyyy-MM-dd; was: %s)\n", dateOfBirth );
               // YOUR CODE  HERE
               break;

            case LENGTH:
               System.out.format( "Enter new length (in m; was: %.2f)", length );
               // YOUR CODE  HERE
               break;

            case WEIGHT:
               System.out.format( "Enter new weight (in kg; was: %.1f)\n", getWeight() );
               // YOUR CODE  HERE
               break;

            default:
               System.out.println( "Invalid entry: " + choice );
               break;
         }
      }
   }

   // Write patient data to screen.
   void printEditMenuOptions()
   {
      System.out.format( "%d: %-17s %s\n", SURNAME, "Surname:", surName );
      System.out.format( "%d: %-17s %s\n", FIRSTNAME, "firstName:", firstName );
      System.out.format( "%d: %-17s %s\n", NICKNAME, "Nickname:", nickName );

      // YOUR CODE HERE TO CALCULATE AGE
      System.out.format( "%d: %-17s %s (age %d)\n", DATEOFBIRTH, "Date of birth:", dateOfBirth, -1 );
      System.out.format( "%d: %-17s %.2f\n", LENGTH, "Length:", length );
      System.out.format( "%d: %-17s %.2f (bmi=%f)\n", WEIGHT, "Weight:", getWeight(), calcBMI() );
   }

   // Write patient data to screen.  TODO: Combine with printEditMenuOptions
   void write()
   {
      System.out.println( "===================================" );
      System.out.format( "%-17s %s\n", "Surname:", surName );
      System.out.format( "%-17s %s\n", "firstName:", firstName );
      System.out.format( "%-17s %s\n", "Nickname:", nickName );

      // YOUR CODE HER TO CALCULATE AGE
      System.out.format( "%-17s %s (age %d)\n", "Date of birth:", dateOfBirth, -1 );
      System.out.format( "%-17s %.2f\n", "Length:", length );
      System.out.format( "%-17s %.2f (bmi=%.1f)\n", "Weight:", getWeight(), calcBMI() );
      System.out.println( "===================================" );
   }

   // Write oneline info of patient to screen
   void writeOneliner()
   {
      System.out.format( "%10s %-20s [%s]", firstName, surName, dateOfBirth.toString() );
      System.out.println();
   }
}
